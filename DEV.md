# Development

## Build

### cross-compile

```bash
export ARCH=arm64
DOCKER_DEFAULT_PLATFORM="linux/${ARCH}" docker run --rm -it -v $PWD:/source -w /tmp --tmpfs /tmp:exec -e DEBUG=true -e CI_COMMIT_TAG=v0.12.7+1 -e CI_PROJECT_DIR=/tmp/source -e NAME=signal-cli-native -e ARCH="${ARCH}" -e CI_JOB_NAME="signal-cli-native-${ARCH}" gcc:10-buster
rm -rf /tmp/source /tmp/libsignal_jni*; cp -r /source . && /tmp/source/.gitlab-ci/build.sh
```
