if ! update-alternatives --query signal-cli | grep -q "%BINARY%"; then
    update-alternatives --install /usr/bin/signal-cli signal-cli /usr/bin/%BINARY% %ORDER%
fi
