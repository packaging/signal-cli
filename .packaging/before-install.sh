getent passwd signal-cli >/dev/null 2>&1 || adduser \
  --system \
  --shell /usr/sbin/nologin \
  --gecos 'signal-cli' \
  --group \
  --disabled-password \
  --home /var/lib/signal-cli \
  signal-cli
