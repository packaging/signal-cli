#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
  set -x
fi

image_variant="$1"

image="${CI_REGISTRY}/${CI_PROJECT_NAMESPACE,,}/${CI_PROJECT_NAME}/${image_variant}"

buildx_args="--progress=plain --provenance=false"
buildx_args+=" --build-arg IMAGE_VARIANT=${image_variant}"
buildx="docker buildx build"
if [[ -n "${CI}" ]]; then
  buildx+=" --output type=local,dest=/tmp/scan-me-please"
else
  buildx+=" --load"
fi
tags="-t ${image}:${CI_COMMIT_REF_SLUG}"

if [[ -n "${CI_COMMIT_TAG}" ]] || [[ "${CI_COMMIT_BRANCH}" == "master" ]]; then
  tags+=" -t ${image}:latest"
fi

# build and push/load
if docker pull "${image}":latest; then
  buildx_args+=" --cache-from ${image}:latest"
fi
# shellcheck disable=SC2086
${buildx} --platform ${PLATFORMS} \
  ${buildx_args} ${tags} \
  -f .docker/Dockerfile .

if ! command -v trivy >/dev/null 2>&1; then
    apk --no-cache add curl
    curl -Lo /tmp/trivy.tar.gz "https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz"
    tar -C /usr/local/bin -xzf /tmp/trivy.tar.gz trivy
fi

platform_to_scan="${PLATFORMS##*,}"
trivy --quiet rootfs --exit-code 10 --severity MEDIUM,HIGH,CRITICAL --no-progress --ignore-unfixed /tmp/scan-me-please/"${platform_to_scan/\//_}"
# shellcheck disable=SC2086
docker buildx build --push --platform ${PLATFORMS} \
  ${buildx_args} ${tags} \
  -f .docker/Dockerfile .
