#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

export DEBIAN_FRONTEND=noninteractive
tag="${CI_COMMIT_TAG:-v0.0.0+0}"
version="${tag%%+*}"
patchlevel="${tag##*+}"
if [[ "${version}" =~ ^@.*$ ]]; then
    signal_cli_branch="${version/@/}"
fi
graalvm="${GRAALVM_VERSION:-21.0.2}"
java="${JAVA_VERSION:-java21}"
architecture="${ARCH:-amd64}"
apt-get -qq update
apt-get -qqy install \
    curl
case "${CI_JOB_NAME}" in
    "signal-cli-native"*|"signal-cli-jre"*)
        case "${architecture}" in
            "amd64")
                libsignal_jni=libsignal_jni_amd64.so
                libsignal_exquo_arch=x86_64
                ;;
            "arm64")
                libsignal_exquo_arch=aarch64
                ;;
        esac
        if [ ! -f "/tmp/${libsignal_jni:-libsignal_jni.so}" ]; then
            if [ ! -d "/tmp/signal-cli-${version//v/}" ]; then
                curl -sfLo "/tmp/signal-cli-${version//v/}.tar.gz" \
                    "https://github.com/AsamK/signal-cli/releases/download/${version}/signal-cli-${version//v/}.tar.gz"
                tar -C /tmp -xzf "/tmp/signal-cli-${version//v/}.tar.gz"
            fi
            libsignalclient="$(find "/tmp/signal-cli-${version//v/}/lib" -type f -name 'libsignal-client-*.jar')"
            if [ -z "${libsignalclient}" ]; then
                printf "\e[1;32mERROR: unable to determine libsignal-client\e[0m\n"
                exit 1
            fi
            libsignalclient_version="${libsignalclient//.jar/}"
            libsignalclient_version="${libsignalclient_version##*-}"
            if [ "${LIBSIGNALCLIENT_DISTRIBUTION:-morph027}" == "exquo" ]; then
                libsignalclient_download_url="https://github.com/exquo/signal-libs-build/releases/download/libsignal_v${libsignalclient_version}/libsignal_jni.so-v${libsignalclient_version}-${libsignal_exquo_arch}-unknown-linux-gnu.tar.gz"
                curl -sfLo "/tmp/libsignal_jni_${libsignalclient_version}.tgz" "${libsignalclient_download_url}"
                tar -C /tmp -xzf "/tmp/libsignal_jni_${libsignalclient_version}.tgz" libsignal_jni.so
            else
                libsignalclient_download_url="https://gitlab.com/packaging/libsignal-client/-/jobs/artifacts/v${libsignalclient_version}/raw/libsignal-client/${architecture}/libsignal_jni.so?job=libsignal-client-${architecture}"
                curl -sfLo /tmp/libsignal_jni.so "${libsignalclient_download_url}"
            fi
            if [[ -n "${libsignal_jni}" ]]; then
                mv /tmp/libsignal_jni.so /tmp/"${libsignal_jni}"
            fi
        else
            libsignalclient_version="$(strings "/tmp/${libsignal_jni:-libsignal_jni.so}" | grep -oE "version:[0-9]+\.[0-9]+\.[0-9]+")"
            libsignalclient_version="${libsignalclient_version##*:}"
        fi
        if ldd "/tmp/${libsignal_jni:-libsignal_jni.so}" | grep --color=never -i "not found"; then
            printf "\e[1;32mERROR: current glibc does not fulfill all libsignal-client requirements\e[0m\n"
            exit 1
        fi
        ;;
esac
printf "\e[1;36mBuild configuration:\e[0m\n"
printf "\e[0;36mArchitecture: %s\e[0m\n" "${architecture}"
printf "\e[0;36mGraalVM (native builds): %s\e[0m\n" "${graalvm}"
printf "\e[0;36mJava: %s\e[0m\n" "${java}"
printf "\e[0;36mlibsignal-client: %s\e[0m\n" "${libsignalclient_version}"

apt-get -qqy install \
    ruby-dev \
    ruby-ffi \
    git \
    binutils \
    zip
type fpm >/dev/null 2>&1 || (
    gem install dotenv -v 2.8.1 --no-doc
    gem install fpm --no-doc
)

tmpdir="$(mktemp -d)"

case "${NAME}" in
"signal-cli-native")
    apt-get -y install \
        build-essential \
        libz-dev
    description="signal-cli provides an unofficial commandline and dbus interface for signalapp/libsignal-service-java - native build"
    case "${architecture}" in
        "amd64")
            graalvm_arch="x64"
            march="x86-64-v2"
            ;;
        "arm64")
            graalvm_arch="aarch64"
            march="compatibility"
            ;;
    esac
    export march
    graalvm_url="https://github.com/graalvm/graalvm-ce-builds/releases/download/jdk-${graalvm}/graalvm-community-jdk-${graalvm}_linux-${graalvm_arch:-${architecture}}_bin.tar.gz"
    graalvm_dir="$(find /tmp -maxdepth 1 -type d -name "graalvm-community-openjdk-${graalvm}+*" || echo /tmp/none)"
    mkdir -p "${tmpdir}/usr/bin"
    if [ ! -d "${graalvm_dir}" ]; then
        curl -Lo "/tmp/${graalvm_url##*/}" "${graalvm_url}"
        tar -C /tmp -xzvf "/tmp/${graalvm_url##*/}" > /tmp/graalvm_unpack.log
        graalvm_dir="$(head -n1 /tmp/graalvm_unpack.log)"
        graalvm_dir="/tmp/${graalvm_dir%%/*}"
    fi
    export PATH="${graalvm_dir}/bin:${PATH}"
    export JAVA_HOME="${graalvm_dir}"
    if [ ! -d /tmp/signal-cli ]; then
        git clone -b "${signal_cli_branch:-${version}}" https://github.com/AsamK/signal-cli /tmp/signal-cli
    fi
    cd /tmp/signal-cli
    git reset --hard
    # slightly update version to make clear this is one is packaged from upstream
    sed -i 's,version = "'"${version//v/}"'",version = "'"${version//v/}+morph027+${patchlevel}"'",' build.gradle.kts
    find "${CI_PROJECT_DIR}/.patches" -type f -name '*.diff' -print0 | xargs --null -L1 git apply
    if [ -n "${REFLECT_CONFIG}" ]; then
        curl -L "${REFLECT_CONFIG}" \
            >/tmp/signal-cli/graalvm-config-dir/reflect-config.json
    fi
    if [ ! -f "lib/src/main/resources/${libsignal_jni:-libsignal_jni.so}" ]; then
        cp -v "/tmp/${libsignal_jni:-libsignal_jni.so}" "lib/src/main/resources/${libsignal_jni:-libsignal_jni.so}"
    fi
    if [ ! -f build/native/nativeCompile/signal-cli ]; then
        ./gradlew nativeCompile
    fi
    mkdir -p "${tmpdir}/usr/bin"
    cp -v build/native/nativeCompile/signal-cli "${tmpdir}/usr/bin/signal-cli-native"
    cd -
    sed 's,%BINARY%,signal-cli-native,' "${CI_PROJECT_DIR}/.packaging/template.after-install.sh" >/tmp/after-install.sh
    sed 's,%BINARY%,signal-cli-native,' "${CI_PROJECT_DIR}/.packaging/template.after-upgrade.sh" >/tmp/after-upgrade.sh
    sed 's,%BINARY%,signal-cli-native,' "${CI_PROJECT_DIR}/.packaging/template.before-remove.sh" >/tmp/before-remove.sh
    sed -i 's,%ORDER%,10,' /tmp/after-install.sh /tmp/after-upgrade.sh
    fpm_source_dirs="usr"
    fpm_opts="--after-install /tmp/after-install.sh"
    fpm_opts+=" --after-upgrade /tmp/after-upgrade.sh"
    fpm_opts+=" --before-remove /tmp/before-remove.sh"
    fpm_opts+=" --before-upgrade ${CI_PROJECT_DIR}/.packaging/before-upgrade.sh"
    fpm_opts+=" --provides signal-cli"
    fpm_opts+=" --deb-suggests sqlite3"
    ;;
"signal-cli-jre")
    description="signal-cli provides an unofficial commandline and dbus interface for signalapp/libsignal-service-java"
    zip -vj "${libsignalclient}" "/tmp/${libsignal_jni:-libsignal_jni.so}"
    mkdir -p \
        "${tmpdir}/usr/share/signal-cli" \
        "${tmpdir}/usr/bin"
    cd "/tmp/signal-cli-${version//v/}"
    # shellcheck disable=SC2016
    sed 's,APP_HOME=\${app_path.*$,APP_HOME=/usr/share/signal-cli/lib/,' bin/signal-cli >"${tmpdir}/usr/bin/signal-cli-jre"
    chmod +x "${tmpdir}/usr/bin/signal-cli-jre"
    cp -rv lib "${tmpdir}/usr/share/signal-cli/"
    cd -
    sed 's,%BINARY%,signal-cli-jre,' "${CI_PROJECT_DIR}/.packaging/template.after-install.sh" >/tmp/after-install.sh
    sed 's,%BINARY%,signal-cli-jre,' "${CI_PROJECT_DIR}/.packaging/template.after-upgrade.sh" >/tmp/after-upgrade.sh
    sed 's,%BINARY%,signal-cli-jre,' "${CI_PROJECT_DIR}/.packaging/template.before-remove.sh" >/tmp/before-remove.sh
    sed -i 's,%ORDER%,20,' /tmp/after-install.sh /tmp/after-upgrade.sh
    fpm_opts="--depends ${java}-runtime-headless"
    fpm_opts+=" --after-install /tmp/after-install.sh"
    fpm_opts+=" --after-upgrade /tmp/after-upgrade.sh"
    fpm_opts+=" --before-remove /tmp/before-remove.sh"
    fpm_opts+=" --before-upgrade ${CI_PROJECT_DIR}/.packaging/before-upgrade.sh"
    fpm_opts+=" --provides signal-cli"
    fpm_opts+=" --deb-suggests sqlite3"
    fpm_source_dirs="usr"
    ;;
"signal-cli-dbus-service")
    description="signal-cli provides an unofficial commandline and dbus interface for signalapp/libsignal-service-java - DBus system service"
    architecture="all"
    mkdir -p \
        "${tmpdir}/etc/dbus-1/system.d" \
        "${tmpdir}/usr/share/dbus-1/system-services" \
        "${tmpdir}/etc/default"
    cp "${CI_PROJECT_DIR}/.packaging/org.asamk.Signal.conf" "${tmpdir}/etc/dbus-1/system.d/org.asamk.Signal.conf"
    curl -Lo "${tmpdir}/usr/share/dbus-1/system-services/org.asamk.Signal.service" "https://raw.githubusercontent.com/AsamK/signal-cli/${version}/data/org.asamk.Signal.service"
    cp -v "${CI_PROJECT_DIR}/.packaging/default-signal-cli-dbus" "${tmpdir}/etc/default/signal-cli-dbus"
    fpm_source_dirs="etc usr"
    fpm_opts="--depends signal-cli"
    fpm_opts+=" --depends systemd"
    fpm_opts+=" --deb-suggests dbus"
    fpm_opts+=" --deb-systemd ${CI_PROJECT_DIR}/.packaging/signal-cli-dbus.service"
    fpm_opts+=" --after-install ${CI_PROJECT_DIR}/.packaging/dbus-service-after-install.sh"
    ;;
"signal-cli-service")
    description="signal-cli provides an unofficial commandline and dbus interface for signalapp/libsignal-service-java - system service"
    architecture="all"
    mkdir -p \
        "${tmpdir}/etc/default"
    cp -v "${CI_PROJECT_DIR}/.packaging/default-signal-cli" "${tmpdir}/etc/default/signal-cli"
    fpm_source_dirs="etc"
    fpm_opts="--depends signal-cli"
    fpm_opts+=" --depends systemd"
    fpm_opts+=" --deb-systemd ${CI_PROJECT_DIR}/.packaging/signal-cli.service"
    ;;
esac
if [ -n "${signal_cli_branch}" ]; then
    tag="$(git -C /tmp/signal-cli describe --tags --dirty)"
    version="${tag/[a-zA-Z]/}+${signal_cli_branch/@/}"
    output="${CI_PROJECT_DIR}/${CI_JOB_NAME}-testing"
else
    version="${version//[a-zA-Z]/}"
    output="${CI_PROJECT_DIR}/${CI_JOB_NAME}"
fi
mkdir -p "${output}"
chmod +t /tmp
# shellcheck disable=SC2086
fpm \
    --input-type dir \
    --output-type deb \
    --force \
    --name "${NAME}" \
    --package "${output}/${NAME}_${version}+morph027+${patchlevel}_${architecture}.deb" \
    --architecture "${architecture}" \
    --version "${version}+morph027+${patchlevel}" \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --url "https://gitlab.com/packaging/signal-cli" \
    --description "${description}" \
    --deb-recommends morph027-keyring \
    --prefix "/" \
    --chdir "${tmpdir}" \
    --before-install "${CI_PROJECT_DIR}/.packaging/before-install.sh" \
    ${fpm_opts} \
    ${fpm_source_dirs}

# test install
case "${CI_JOB_NAME}" in
    "signal-cli-native"*)
        apt-get -y install "${output}/${NAME}_${version}+morph027+${patchlevel}_${architecture}.deb"
        set +e
        out="$(signal-cli --service-environment sandbox -a "+49341123456" --verbose register 2>&1)"
        set -e
        if ! grep -qE "Captcha|RateLimitException|VerificationMethoNotAvailableException" <<<"$out"; then
            echo "${out}"
            printf "\e[1;32mERROR: unable to run signal-cli\e[0m\n"
            exit 1
        fi
        ;;
esac
