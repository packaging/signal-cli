#!/bin/ash

set -e

apk --no-cache add git openssl
wget "https://github.com/spf13/hugo/releases/download/v${HUGO}/hugo_${HUGO}_Linux-64bit.tar.gz"
tar xf "hugo_${HUGO}_Linux-64bit.tar.gz" && cp hugo /usr/bin/
hugo version
git submodule update --init
cd docs
hugo
mv public "${CI_PROJECT_DIR}"
cp -rv \
    "${CI_PROJECT_DIR}/.repo/gpg.key" \
    "${CI_PROJECT_DIR}/.repo/deb/signalcli/signalcli/dists" \
    "${CI_PROJECT_DIR}/.repo/deb/signalcli/signalcli/pool" \
    "${CI_PROJECT_DIR}/public/"
cp -r "${CI_PROJECT_DIR}"/.public/* "${CI_PROJECT_DIR}/public/"
