+++
title = "Configuration"
weight = 3
+++

### signal-cli-service / signal-cli-dbus-service

### daemon options

Daemon options can be set in `/etc/default/signal-cli` or `/etc/default/signal-cli-dbus` and flags and parameters will be added to `signal-cli` executable.

### DBus permissions

By default, the package setup limits DBus access to user *signal-cli*. If you want to allow other users, create an additional config file and reload dbus.

Example:

```xml
<?xml version="1.0"?> <!--*-nxml-*-->
<!DOCTYPE busconfig PUBLIC "-//freedesktop//DTD D-BUS Bus Configuration 1.0//EN"
        "http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">

<busconfig>
        <!-- allows user 'sendonlyuser' to send messages --!>
        <policy user="sendonlyuser">
                <allow send_destination="org.asamk.Signal"/>
        </policy>
        <!-- allows user 'receiveonlyuser' to receive messages --!>
        <policy user="receiveonlyuser">
                <allow receive_sender="org.asamk.Signal"/>
        </policy>
        <!-- allows user 'sendreceiveuser' to send and receive messages --!>
        <policy user="sendreceiveuser">
                <allow send_destination="org.asamk.Signal"/>
                <allow receive_sender="org.asamk.Signal"/>
        </policy>
</busconfig>
```

Issue an dbus reload afterwards: `systemctl reload dbus.service`
