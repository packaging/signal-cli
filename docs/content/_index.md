# signal-cli Ubuntu/Debian packages

This project creates Ubuntu/Debian packages for [AsamK/signal-cli](https://github.com/AsamK/signal-cli/) including DBus service.

## Package variants

### signal-cli-jre

Contains plain jar files and the start script, requires JRE >= 17 (e.g. `openjdk-17-jre-headless`, will be installed automatically).

### Supported architectures

* amd64 
* arm64

### signal-cli-native - EXPERIMENTAL

Native [GraalVM](https://github.com/AsamK/signal-cli/#building-a-native-binary-with-graalvm-experimental) build (**experimental**).
Provides a single binary file and does not requires JRE.

### signal-cli-service

This package provides a systemd service `signal-cli.service` (**without** account), which can be used as JSON-RPC daemon. See configuration for more details.

### signal-cli-dbus-service

This package provides a ready to use [DBus System Service](https://github.com/AsamK/signal-cli/wiki/DBus-service), which adds a system DBus config and a systemd service `signal-cli-dbus.service` (**without** account), see configuration for more details.
