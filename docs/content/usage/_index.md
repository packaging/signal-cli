+++
title = "Usage"
weight = 5
+++

{{% notice info %}}
Please run all commands as `signal-cli` user when in standalone mode. Not necessary for docker installation.
{{% /notice %}}


## JSON-RPC

When deamon has been started with JSON-RPC (e.g. `SIGNAL_CLI_DAEMON_PARAMS="--http=localhost:3000"` in `/etc/default/signal-cli`), you can use HTTP requests.

### curl

```bash
curl \
-H "content-type: application/json" \
-d'{"jsonrpc":"2.0", "method": "send", "id": "'"${RANDOM}"'", "params": {"account": "+<your-signal-account>", "recipients": ["+<recipient>"], "message": "TEST"}}' \
http://127.0.0.1:3000/api/v1/rpc
```

### python

```python
from requests import post
from uuid import uuid4
res = post(
    "http://127.0.0.1:3000/api/v1/rpc",
    json={
        "jsonrpc":"2.0",
        "method": "send",
        "id": "uuid4().__str__()",
        "params": {
            "account": "+<your-signal-account>",
            "recipients": ["+<recipient>"],
            "message": "Test"
        }
    }
)
```
