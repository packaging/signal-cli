+++
title = "Docker installation"
+++

Docker setup recommended for JSON-RPC via TCP only (DBus won't work in this image).

## Run container

{{< tabs groupId="run" >}}
{{% tab name="Docker" %}}
```bash
docker run -d \
  --name signal-cli \
  --publish 7583:7583 \
  --volume /some/local/dir/signal-cli-config:/var/lib/signal-cli \
  --tmpfs /tmp:exec \
  registry.gitlab.com/packaging/signal-cli/signal-cli-<variant>:latest \
  daemon --tcp 0.0.0.0:7583
```
`<variant>`: `native` or `jre`.
{{% /tab %}}
{{% tab name="Docker Compose" %}}
```yaml
version: "3"
services:
  signal-cli:
    image: registry.gitlab.com/packaging/signal-cli/signal-cli-<variant>:latest
    command: daemon --tcp 0.0.0.0:7583
    ports:
      - "7583:7583"
    volumes:
      - "/some/local/dir/signal-cli-config:/var/lib/signal-cli"
    tmpfs:
      - "/tmp:exec"
```
`<variant>`: `native` or `jre`.
{{% /tab %}}
{{< /tabs >}}

## Secure containers

If you're not running a secured reverse proxy already and just want to secure the traffic to the signal-cli JSON-RPC (you should!), we can use [traefik](https://traefik.io/) and [step-ca](https://smallstep.com/docs/step-ca/), to easily add self-signed ssl certs and basic authentication.

{{< tabs groupId="runsecured" >}}
{{% tab name="Docker Compose" %}}
```yaml
version: "3"
services:
  signal-cli:
    image: registry.gitlab.com/packaging/signal-cli/signal-cli-<variant>:latest
    command: daemon --tcp 0.0.0.0:7583
    volumes:
      - signal-cli-config:/var/lib/signal-cli
    networks:
      - traefik
    tmpfs:
      - "/tmp:exec"
    labels:
      traefik.enable: true
      traefik.http.routers.signal-cli-https.rule: PathPrefix(`/`)
      traefik.http.routers.signal-cli-https.entrypoints: websecure
      traefik.http.routers.signal-cli-https.service: signal-cli-https
      traefik.http.services.signal-cli-https.loadbalancer.server.port: 7583
      traefik.http.services.signal-cli-https.loadbalancer.server.scheme: http
  step-ca:
    image: smallstep/step-ca:0.25.2
    volumes:
      - step-ca:/home/step
    environment:
      DOCKER_STEPCA_INIT_NAME: "Step CA"
      DOCKER_STEPCA_INIT_DNS_NAMES: "localhost,step-ca"
      DOCKER_STEPCA_INIT_REMOTE_MANAGEMENT: "true"
      DOCKER_STEPCA_INIT_ACME: "true"
    networks:
      - traefik
  traefik:
    depends_on:
      - step-ca
    image: traefik:2.10
    command:
      - '--providers.docker=true'
      - '--providers.docker.network=traefik'
      - '--providers.docker.exposedByDefault=false'
      - '--api.dashboard=true'
      - '--api.insecure=true'
      - '--accesslog=true'
      - '--pilot.dashboard=false'
      - '--entryPoints.web.address=:80'
      - '--entryPoints.web.http.redirections.entryPoint.to=websecure'
      - '--entrypoints.web.http.redirections.entryPoint.scheme=https'
      - '--entrypoints.web.http.redirections.entrypoint.permanent=true'
      - '--entryPoints.websecure.address=:443'
      - '--entrypoints.websecure.http.tls.certResolver=step-ca'
      - '--certificatesresolvers.step-ca.acme.caserver=https://step-ca:9000/acme/acme/directory'
      - '--certificatesresolvers.step-ca.acme.email=traefik@localhost.localdomain'
      - '--certificatesresolvers.step-ca.acme.tlsChallenge=true'
    environment:
      LEGO_CA_CERTIFICATES: /home/step/certs/root_ca.crt
    networks:
      - traefik
    ports:
      - target: 80
        published: 80
        mode: host
      - target: 443
        published: 443
        mode: host
    volumes:
      - step-ca:/home/step
      - /var/run/docker.sock:/var/run/docker.sock
    labels:
      traefik.enable: true
      traefik.http.routers.traefik-https.rule: Host(`traefik.localhost`)
      traefik.http.routers.traefik-https.entrypoints: websecure
      traefik.http.routers.traefik-https.service: api@internal
networks:
  traefik: {}
volumes:
  step-ca: {}
  signal-cli-config: {}
```
`<variant>`: `native` or `jre`.
{{% /tab %}}
{{< /tabs >}}
