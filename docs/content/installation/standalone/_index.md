+++
title = "Standalone installation"
+++

## Repository

{{% notice info %}}
Minimum glibc version is 2.31 due to libsignal-client

Minimum CPU hardware capabilities for x86-64 is v2 (`/lib64/ld-linux-x86-64.so.2 --help | grep supported`)
{{% /notice %}}

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-signal-cli.asc https://packaging.gitlab.io/signal-cli/gpg.key
```

### Add repo to apt

```bash
echo "deb https://packaging.gitlab.io/signal-cli signalcli main" | sudo tee /etc/apt/sources.list.d/morph027-signal-cli.list
```

## Packages

### signal-cli

You will either need **_signal-cli-jre_** or **_signal-cli-native_** before you can use **_signal-cli-service_**/**_signal-cli-dbus-service_**.

Both packages are co-installable and the active one can be selected using `sudo update-alternatives --config signal-cli`.

```bash
sudo apt-get install signal-cli-<jre|native> morph027-keyring
```

### signal-cli-service (JSON-RPC)

```bash
sudo apt-get install signal-cli-service
```

### signal-cli-dbus-service (DBus)

```bash
sudo apt-get install signal-cli-dbus-service
```

## Extras

### unattended-upgrades

To enable automatic upgrades using `unattended-upgrades`, just add the following config file:

```bash
cat > /etc/apt/apt.conf.d/50signalcli <<EOF
Unattended-Upgrade::Allowed-Origins {
	"morph027:signalcli";
};
EOF
```
