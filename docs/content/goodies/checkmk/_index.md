+++
title = "Check MK Notifications"
weight = 1
+++

## Setup

### Virtualenv

Create a virtualenv:

```bash
SITE="<your-site-name>"
sudo -u "$SITE" -H /usr/bin/python3 -m virtualenv -p /usr/bin/python3 "/omd/sites/$SITE/.virtualenv"
sudo -u "$SITE" -H /usr/bin/python3 -m virtualenv -p /usr/bin/python3 "/omd/sites/$SITE/.virtualenv/bin/pip" install -U pysignalclijsonrpc
```

## Plugin

Create a file `/omd/sites/<site>/local/share/check_mk/notifications/pysignaljsonrpc`:

```python
#!/omd/sites/<your-site-name>/.virtualenv/bin/python
# Signal
# Bulk: no

import os
import sys

from pysignalclijsonrpc.api import SignalCliJSONRPCApi

message = ""
if os.environ.get("NOTIFY_WHAT") == "SERVICE":
    if os.environ.get("NOTIFY_SERVICESTATE") == "OK":
        message = "✅"
    elif os.environ.get("NOTIFY_SERVICESTATE") == "WARNING":
        message = "⚠️"
    elif os.environ.get("NOTIFY_SERVICESTATE") == "CRITICAL":
        message = "‼️"
    elif os.environ.get("NOTIFY_SERVICESTATE") == "UNKNOWN":
        message = "❔"
    message = f'{message} {os.environ.get("NOTIFY_HOSTNAME")} {os.environ.get("NOTIFY_SERVICESTATE")} {os.environ.get("NOTIFY_SERVICEDESC")} {os.environ.get("NOTIFY_SERVICEOUTPUT")}'
elif os.environ.get("NOTIFY_WHAT") == "HOST":
    if os.environ.get("NOTIFY_HOSTSTATE") == "UP":
        message = "✅"
    elif os.environ.get("NOTIFY_HOSTSTATE") == "DOWN":
        message = "‼️"
    message = f'{message} {os.environ.get("NOTIFY_HOSTNAME")} is {os.environ.get("NOTIFY_HOSTSTATE")}'

recipient = os.environ.get("NOTIFY_CONTACTPAGER")

if not recipient:
    sys.stderr.write(
        "Error: No contact pager set for %s\n" % os.environ.get("NOTIFY_CONTACTNAME")
    )
    sys.exit(1)

signalcli = SignalCliJSONRPCApi(
    endpoint=f"{os.environ.get('NOTIFY_PARAMETER_1')}",
    account=f"{os.environ.get('NOTIFY_PARAMETER_2')}",
)
signalcli.send_message(recipients=[recipient], message=message)
```

## User notifications

* In the sidebar, click on your user and select "Notification rules"
* Add rule
* Description: Signal
* Notification Method: Signal
* Call with the following parameters:
  * http://<<your-signal-cli-jsron-rpc-host-and-port>>/api/v1/rpc
  * <<your phone number>>

![](/signal-cli/img/checkmk.png)
